#  XC888 Lib  {#mainpage} #

Dit is de documentatie van de XC888 lib.

![XC888](..\resources\xc888.png "XC888")

## Situering ##

De lib is geschreven door Wim Dams voor de XC888 Microcontroller van Marc Roggemans en Danny Pauwels.  

Er zijn 2 versies van dit controller systeem:

 - De standaard die gebruikt wordt bij de lessen microcontrollers in het 1ste jaar. (links op de afbeelding) Informatie hierover kan men terug vinden op: <ftp://telescript.denayer.wenk.be/pub/CD-Microcontrollers/8051/XC888>
 - De DIL versie (Rechts in beeld). Deze versie is minder gekend maar is ideaal als de vormfactor van het standaard bord voor een bepaald project te groot zou zijn. Deze versie kan nog steeds via USB geprogrammeerd worden via de on-board FTDI (USB-to-Serial bridge). De print hiervan kan men bekomen via Mr. Roggemans en de componenten zijn te verkrijgen in het winkeltje van het school. De schema's: <ftp://telescript.denayer.wenk.be/pub/CD-Microcontrollers/8051/XC888DIL/XC888_dil52_docu.pdf>

De lib zou het eenvoudiger moeten maken om de I/O van het bord vanuit C aan te sturen.

Bekijk zeker de [Voorbeelden](@ref Voorbeelden) en de [Getting Started pagina](@ref md_doc_markdown_Getting_Started) om snel gestart te geraken.


## Versies
- De lib kan gecompileerd worden met sdcc 2.9 of 3.8 andere versies zijn niet getest.
- De projectfiles (*.wsp) kunnen geopend worden met Silicon Labs Integrated Development Environment versie 4.90.00
- De gegenereerde hex file kan worden ingeladen worden met XC800_FLOAD   
(deze kan worden geïntegreerd in Silicon Labs IDE)

zie [de Installatie pagina](@ref md_doc_markdown_Installatie) voor meer info over de software 