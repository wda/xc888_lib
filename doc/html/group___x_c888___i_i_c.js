var group___x_c888___i_i_c =
[
    [ "IIC_ACK", "group___x_c888___i_i_c.html#gaf18fdbba37cfa33a5c70fcbf990e87bb", null ],
    [ "IIC_NACK", "group___x_c888___i_i_c.html#ga685ac15315dcd86c6c1d73de3c9df64b", null ],
    [ "scl", "group___x_c888___i_i_c.html#ga25124161a0939cc094d077ada40d97ed", null ],
    [ "sda", "group___x_c888___i_i_c.html#gab3e898731990cccbe6290a096108e0bd", null ],
    [ "iicinbyte", "group___x_c888___i_i_c.html#gabbadf816fba418f1f34140376f2f8aad", null ],
    [ "iicoutbyte", "group___x_c888___i_i_c.html#ga62f8366c1643e7e91d887f76056fde46", null ],
    [ "iicstart", "group___x_c888___i_i_c.html#gaf83ef9af4245719a107e7a35caa0576f", null ],
    [ "iicstop", "group___x_c888___i_i_c.html#gad1915d1232d1fcdcda55c7f0473b3ca9", null ],
    [ "initiic", "group___x_c888___i_i_c.html#ga01a9920867a6586b0b4776e1d225273b", null ]
];