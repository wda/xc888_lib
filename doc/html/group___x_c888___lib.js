var group___x_c888___lib =
[
    [ "XC888 Common", "group___x_c888__common.html", "group___x_c888__common" ],
    [ "XC888 Delay", "group___x_c888__delay.html", "group___x_c888__delay" ],
    [ "XC888 Serial/UART", "group___x_c888__serieel.html", "group___x_c888__serieel" ],
    [ "XC888 IIC", "group___x_c888___i_i_c.html", "group___x_c888___i_i_c" ],
    [ "XC888 LCD", "group___x_c888___l_c_d.html", "group___x_c888___l_c_d" ],
    [ "XC888 GPIO", "group___x_c888___g_p_i_o.html", "group___x_c888___g_p_i_o" ],
    [ "XC888 ADC", "group___x_c888___a_d_c.html", "group___x_c888___a_d_c" ],
    [ "XC888 PWM", "group___x_c888___p_w_m.html", "group___x_c888___p_w_m" ],
    [ "XC888 SPI", "group___x_c888___s_p_i.html", "group___x_c888___s_p_i" ],
    [ "XC888 Interrupts", "group___x_c888___i_n_t.html", "group___x_c888___i_n_t" ]
];