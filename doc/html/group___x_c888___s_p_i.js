var group___x_c888___s_p_i =
[
    [ "SPI_100K_BAUD", "group___x_c888___s_p_i.html#ga8830ad0a14ed84be657809bfbabee7e4", null ],
    [ "SPI_12M_BAUD", "group___x_c888___s_p_i.html#ga624a7bf5818d71d93c02cea66a1c84ff", null ],
    [ "SPI_133K3_BAUD", "group___x_c888___s_p_i.html#gaf905f7951c77e6da69f07f9a42705703", null ],
    [ "SPI_1M3_BAUD", "group___x_c888___s_p_i.html#ga8e8d485997fed1fb3433a9c248fa3e1f", null ],
    [ "SPI_1M_BAUD", "group___x_c888___s_p_i.html#ga6cb385ad2be6371025dc1386b33a632c", null ],
    [ "SPI_200K_BAUD", "group___x_c888___s_p_i.html#gafd699fdd8264fafd40f2a6dd011c128e", null ],
    [ "SPI_266K7_BAUD", "group___x_c888___s_p_i.html#gac2e53c14f13d8bbc01a406225a6824aa", null ],
    [ "SPI_500K_BAUD", "group___x_c888___s_p_i.html#ga863c58d4a153084ece0a875929ba669d", null ],
    [ "SPI_600K_BAUD", "group___x_c888___s_p_i.html#gaf6d6190c7da9ed5251be3e54e22a64be", null ],
    [ "SPI_666K7_BAUD", "group___x_c888___s_p_i.html#gaac4a2453049eb3e219e2920d0cc1dcfd", null ],
    [ "SPI_6M_BAUD", "group___x_c888___s_p_i.html#gab907acccf80b047ef27f029f19d831c2", null ],
    [ "SPI_750K_BAUD", "group___x_c888___s_p_i.html#gaf6d38f66a8b522fcb3626ca3c9542e5a", null ],
    [ "SPI_CS", "group___x_c888___s_p_i.html#gade4259fa3cbb71732a4e73c18dcb9b0d", null ],
    [ "SPI_LSB_FIRST", "group___x_c888___s_p_i.html#ga8d194446c8ac9f3c878ead3c9382c9e4", null ],
    [ "SPI_MODE00", "group___x_c888___s_p_i.html#ga4209ed80f8dd8edef65e8adf451e0a33", null ],
    [ "SPI_MODE01", "group___x_c888___s_p_i.html#ga66ca5c7208f5f9a1ece10f020c503455", null ],
    [ "SPI_MODE10", "group___x_c888___s_p_i.html#gab42b9efafe8f259ce177d5d4878c5196", null ],
    [ "SPI_MODE11", "group___x_c888___s_p_i.html#ga6e9c10f59c6fb3329f13a904ea030815", null ],
    [ "SPI_MSB_FIRST", "group___x_c888___s_p_i.html#ga701fce0a1e220262d9912911e87c9e00", null ],
    [ "initspi", "group___x_c888___s_p_i.html#gae82e6c378c11be665a4f8240265fb877", null ],
    [ "spioutbyte", "group___x_c888___s_p_i.html#gac0ed01a67f43876e5a990b29f32dcb40", null ]
];