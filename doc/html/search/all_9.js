var searchData=
[
  ['i2ceeprom_2ec',['I2CEEPROM.c',['../_i2_c_e_e_p_r_o_m_8c.html',1,'']]],
  ['iic_5fack',['IIC_ACK',['../group___x_c888___i_i_c.html#gaf18fdbba37cfa33a5c70fcbf990e87bb',1,'xcez_lib.h']]],
  ['iic_5fnack',['IIC_NACK',['../group___x_c888___i_i_c.html#ga685ac15315dcd86c6c1d73de3c9df64b',1,'xcez_lib.h']]],
  ['iicinbyte',['iicinbyte',['../group___x_c888___i_i_c.html#gabbadf816fba418f1f34140376f2f8aad',1,'xcez_lib.c']]],
  ['iicoutbyte',['iicoutbyte',['../group___x_c888___i_i_c.html#ga62f8366c1643e7e91d887f76056fde46',1,'xcez_lib.c']]],
  ['iicstart',['iicstart',['../group___x_c888___i_i_c.html#gaf83ef9af4245719a107e7a35caa0576f',1,'xcez_lib.c']]],
  ['iicstop',['iicstop',['../group___x_c888___i_i_c.html#gad1915d1232d1fcdcda55c7f0473b3ca9',1,'xcez_lib.c']]],
  ['initadcs',['initadcs',['../group___x_c888___a_d_c.html#gac1b1f2cd7852cfaeb13c089835d27d21',1,'xcez_lib.c']]],
  ['initdipswitch',['initdipswitch',['../group___x_c888___g_p_i_o.html#ga8081aecdab96428fec377c728932f9dc',1,'xcez_lib.c']]],
  ['initftoetsen',['initftoetsen',['../group___x_c888___g_p_i_o.html#ga458e771edf4371619f08478311077860',1,'xcez_lib.c']]],
  ['initiic',['initiic',['../group___x_c888___i_i_c.html#ga01a9920867a6586b0b4776e1d225273b',1,'xcez_lib.c']]],
  ['initlcd',['initlcd',['../group___x_c888___l_c_d.html#gad275ab846286640babdec223bd254f63',1,'xcez_lib.c']]],
  ['initleds',['initleds',['../group___x_c888___g_p_i_o.html#ga8b13856a48725cb1eb0179878f6ce799',1,'xcez_lib.c']]],
  ['initpwm',['initpwm',['../group___x_c888___p_w_m.html#ga401dd32585e46f1027217c211ce7540b',1,'xcez_lib.c']]],
  ['initsio',['initsio',['../group___x_c888__serieel.html#ga5e600eb5295ad54dce439c4ea3d8f152',1,'xcez_lib.c']]],
  ['initspi',['initspi',['../group___x_c888___s_p_i.html#gae82e6c378c11be665a4f8240265fb877',1,'xcez_lib.c']]],
  ['installatie_2emd',['Installatie.md',['../_installatie_8md.html',1,'']]],
  ['interrupt_2ec',['interrupt.c',['../examples_207_01_interrupts_2interrupt_8c.html',1,'(Global Namespace)'],['../projects_2volatile_2interrupt_8c.html',1,'(Global Namespace)']]],
  ['io_5fnot_5fpressed',['IO_NOT_PRESSED',['../group___x_c888___g_p_i_o.html#ga0ab6ce9c031e43f1b8a3030bf5a75e34',1,'xcez_lib.h']]],
  ['io_5fpressed',['IO_PRESSED',['../group___x_c888___g_p_i_o.html#ga3020b30b0990e1aca14f5a7526844753',1,'xcez_lib.h']]],
  ['installatie',['Installatie',['../md_doc_markdown__installatie.html',1,'']]]
];
