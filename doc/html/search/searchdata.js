var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvx",
  1: "l",
  2: "abcghimnprstx",
  3: "_adgilmopst",
  4: "bdefor",
  5: "lrtu",
  6: "vx",
  7: "gimtx"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "groups",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Modules",
  7: "Pages"
};

