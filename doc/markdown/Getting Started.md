# Getting started

# Kijk eerst naar een voorbeeld.

In de map examples zijn er voorbeelden die je rechtstreeks kan openen in de silicon labs IDE. Navigeer naar de map "examples" open de map met het gewenste voorbeeld en dubbelklik op het *.wsp bestand.

# Start een nieuw Leeg project.

Via de menu kies je voor "project" -> "New Project"

![New Project](..\resources\01_new_project.png "New Project")

Hier kies je:

 - De naam van je project
 - De locatie op je computer deze moet op de juiste plaats staan t.o.v. de lib.
  - Maak een map aan naast de map examples met als naam "my projects"
  - Maak per project een nieuwe map aan met dezelfde naam als de naam van je project
 
![New Project Window](..\resources\02_new_project_window.png "New Project Window")

Voeg een nieuwe groep toe voor de lib. Doe dit door rechts te klikken op de naam van het project en te kiezen voor "Add Groups to project myFirstApp"

![Add Group](..\resources\03_add_group.png "Add Group")

Daarna voeg je bestanden toe aan de groepen. Doe dit door rechts te klikken op de  groep en te kiezen voor "Add file to group ..." doe dit voor:
 - xcez_lib.c uit de map lib
 - xc888.h uit de map include
 - xcez_lib.h uit de map include

zie verder voor een screenshot hoe het er dan moet uitzien.

![Add Files](..\resources\04_add_files.png "Add Files")

Als alles is goed gegaan ziet het er als volgt uit:

![Add Files Done](..\resources\05_add_files_done.png "Add Files Done")

De volgende stap is een nieuw C bestand toevoegen aan het project.
Selecteer de groep/map "Source Files" en kies dan in de menu "File" -> "New file"

Kies een naam (liefst geen spaties) en zorg ervoor dat de checkboxen "Add to project" en "Add to build" alle 2 zijn aangevinkt.

Kijk ook na dat het nieuwe bestand wordt opgeslagen in de juiste map (juiste map = project map).

![New C File](..\resources\06_new_c_file.png "New C File")

In het screenshot hieronder zie je een verschillend icoon voor xcez_lib.c als voor het nieuwe c bestand. Dit is omdat de lib nog niet is toegevoegd aan de "build" en zal nu dus niet gecompileerd worden. xcez_lib moet dus nog toegevoegd worden aan de build. Doe dit door er rechts op te klikken en dan te kiezen voor "Add xcez_lib.c to build".

![Add to build](..\resources\07_add_to_build.png "Add to build")

Het volgende is het selecteren/ingeven van de juiste toolchain.
Kies in de menu "Project" -> "Tool Chain Integration..."
    
![Toolchain](..\resources\08_toolchain.png "Toolchain")

Indien dit de eerste keer is dat deze menu opengaat moet je een nieuwe preset maken op basis van de preset "SDCC 3.x (read only)":

 - Selecteer de preset "SDCC 3.x (read only)"
 - Ga naar het tabblad compiler en pas deze aan tot je volgende "Command line flags" krijgt: `-c --debug --use-stdout -V -I"..\include" -I"..\..\include" --xram-loc 0xF000 --xram-size 0x600 --code-size 0x5000`
 - Ga naar het tabblad linker en pas deze aan tot je volgende "Command line flags" krijgt: `--debug --use-stdout -V --xram-loc 0xF000 --xram-size 0x600 --code-size 0x5000`

Sla deze preset op onder de naam "SDCC3 XC888".
Op die manier kan je de volgende keer gewoon de preset "SDCC XC888" selecteren.

![Toolchain Compiler](..\resources\09_toolchain_compiler.png "Toolchain Compiler")

![Toolchain Linker](..\resources\10_toolchain_linker.png "Toolchain Linker")

Voeg de code toe aan je eigen source file. Zorg ervoor dat hier steeds de volgende includes staan: 

 - xcez_lib.h deze bevat de declaratie van alle functies van de bibliotheek
 - xc888.h deze bevat alle register definities (namen + adressen) van de XC888 controller.

Zorg dat er in de main een oneindige lus is en dat voor deze lus alle initialisatie gebeurd.

Dus voor elke peripheral die men wenst te gebruiken moet er nagegaan worden of er initialisatie functies/code moet worden toegevoegd.

Voor printf moet `initsio();` opgeroepen worden aangezien deze zijn output stuurt via de uart. (sio = serial in out)

![Code Toevoegen](..\resources\11_code_toevoegen.png "Code Toevoegen")

Compileren kan je doen via het volgende icoontje

![Compileren](..\resources\12_compileren.png "Compileren")

Dit kan je te zien krijgen als er een compilatie error is.
Scroll dan eerst helemaal naar boven en los de eerste error op en probeer opnieuw.

![Error](..\resources\13_compile_error.png "Error")

Dit zie je als er geen errors zijn:

![OK](..\resources\13_compile_geen_error.png "OK")

Dan kan je het hex bestand in de controller laden via XC800_Fload:

 - Kies als Target Device XC888-8FF 
 - Kies de juiste COM poort (zie apparaatbeheer)
 - Klik op "Open File" en selecteer het hex bestand in je project map.
 - Op de controller druk je op reset en laat je reset los terwijl boot nog ingedrukt is.
 - Dan verder in XC800_Fload klik je op download
 - Druk op Execute Flash of op reset om het nieuwe programma te laten lopen.

![Programma naar controller laden](..\resources\14_flash_load.png "Programma naar controller laden")

