# Installatie #

Hier word beschreven welke software er moet worden geïnstalleerd om applicaties te kunnen ontwikkelen voor de XC888.

## Software verkrijgen

De volgende software is aangeraden:

 - Silicon Labs IDE versie 4.90.00 <br>
Dit is de ontwikkelomgeving waarin men code kan schrijven. De toolchain moet echter appart geïnstalleerd worden (zie SDCC). <br>
<https://www.silabs.com/Support%20Documents/Software/mcu_ide.exe>

 - XC800_FLOAD V4.7 <br>
Dit is de tool om het intel-hex bestand in te laden in de controller. <br>
<ftp://telescript.denayer.wenk.be/pub/CD-Microcontrollers/8051/XC888/software/FLOAD/setup_v4.7.exe>

 - SDCC versie 3.8.0 <br>
Dit is de toolchain (m.a.w. de compiler) die de code (zowel C als ASM) kan compileren. <br>
Installeer deze in het standaard path zodat de IDE deze gemakkelijk kan vinden.<br>
<http://sdcc.sourceforge.net/index.php#Download>

De volgende software is optioneel:

 - Putty <br>
Deze tool kan de seriele poort openen waardoor men deze als debug kanaal kan gebruiken (geen ssh of telnet). De output van printf wordt dan via de seriele poort naar deze console gestuurd.
Wel even de juiste COM poort ingeven met 9600 (8N1) als baudrate en aan de kant van de controller moet initsio opgeroepen worden.
<http://the.earth.li/~sgtatham/putty/latest/x86/putty.exe>