# Memory 

Dit moet toch minimaal 1 keer gelezen zijn _voor_ je begint te programmeren.

## Memory model van een 8051 (de core van de XC888)

Zoals je wellicht gezien hebt bij Mr. Roggemans heeft de 8051 core 3 "soorten" geheugens. 

 1. Internal Ram of m.a.w. de registers (\b __idata). Dit zijn geheugenplaatsen van 8 bit groot en zitten zeer dicht bij de CPU. Binnen deze soort kan men nog eens onderscheid maken tussen "General purpose registers" (GPR) en "Special function registers" (SFR). 
  - De GPR kan je vrij gebruiken voor dingen op te slagen en terug te lezen. meestal zit hier ook de stack. Als je dan weet dat deze GPR in totaal 256 bytes groot is weet je dat je hier snel te kort zal hebben. Men maakt onderscheid tussen de bovenste helft en de onderste helft omdat enkel de onderste helft direct adresseerbaar is (\b __data).
  - De SFR zijn registers die gekoppeld zijn aan hardware als je hier naar schrijft/leest beinvloed je de hardware die aan deze registers gekoppeld is. Dit kunnen timers, poorten of andere periferie zijn. 

 2. Programma geheugen (\b __code) ook wel code memory genoemd bevat de eigenlijke instructies van het programma. Dit is ofwel ROM of Flash geheugen omdat deze bewaard moeten blijven bij het onderbreken van de voeding.
 3.  Data geheugen (\b __xdata), Data memory of XRAM genoemd kan men gebruiken om data (variabelen) op te slagen en is meestal beduidend groter dan de interne ram. De X van XRAM duid erop dat deze eXtern zou zijn wat vroeger ook effectief het geval was. Maar moderne varianten van de 8051 zit dit altijd intern in de chip.

## Concreet voor de XC888

De XC888 komt in meerdere uitvoeringen. Voor het XC888 bord kan dit een variant zijn met 24Kbyte Flash of één met 32Kbyte Flash.

Hieronder zie je de memory map zoals ze terug te vinden is in de Datasheet van de XC888. De delen in het grijs zijn niet beschikbaar in een 24Kbyte variant.
![Memory map](..\resources\memory_map.png "XC888 Memory map")

Een detail van het flash gedeelte waar duidelijk het verschil te zien is tussen de varianten.
![Flash Memory map](..\resources\flash_memory_map.png "XC888 Flash Memory map")
### Voor de XC888 met 24Kbyte geheugen  
| Soort geheugen        	| Start adres           | hoe groot		|
| ---						| --- 					| --- 			|
| Data MEM (__xdata)		| 0xF000 				| 0x600 (1,5KB)	|
| Code MEM (__code)			| 0x0000 				| 0x5000 (20KB)	|
| Data Flash (nt std 8051)	| 0x7000 				| 0x1000 (4KB)	|


### Voor de XC888 met 32Kbyte geheugen  
| Soort geheugen        	| Start adres           | hoe groot		|
| ---						| --- 					| --- 			|
| Data MEM (__xdata)		| 0xF000 				| 0x600 (1,5KB)	|
| Code MEM (__code)			| 0x0000 				| 0x7000 (28KB)	|
| Data Flash (nt std 8051)	| 0xB000 				| 0x1000 (4KB)	|

(De Data flash kan ook als Code Mem gebruikt worden)

### Deze informatie doorgeven aan de compiler

Deze informatie kan men doorgeven aan de compiler via de volgende command line opties:
    `--xram-loc 0xF000 --xram-size 0x600 --code-size 0x5000`

## Variabelen in een bepaald soort geheugen plaatsen


### Constanten

(Grote) constanten of variabelen die niet worden gewijzigd kunnen best in het code memory staan (flash). Dit kan je doen op de volgende manier:

__code uint16_t bufferSize = 1024;

Zeker voor "grote" datasets kan dit interessant zijn:

    __code char htmlCode[] = "<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>Hello World</title></head><body><h1>Hello from XC888</h1></body></html>";

### grote variabelen 

"Grote" variabelen staan best in XRAM doe dit met op de volgende manier:

__xdata uint8_t buffer[50];
__xdata uint32_t counter;

## Waarschuwing!  SDCC & Reentrant functions

Als je SDCC gebruikt zijn functies standaard niet reentrant en zolang je geen interrupts gebruikt is dit totaal niet erg. 

Wat is een reentrant functie (in deze context)? 

Dit is een functie die in het midden van zijn uitvoering mag onderbroken worden (door een interrupt) en opnieuw mag worden opgeroepen vanuit de interrupt routine. Dit kan enkel als de functie elke keer unieke data gebruikt. Typisch wordt dit gedaan door de lokale variabelen op de stack te plaatsen. Op die manier is de data verschillend als de functie voor de 2de keer opgeroepen wordt. Alleen kiest SDCC er soms/meestal voor om lokale variabelen en zelfs argumenten niet op de stack te plaatsen maar in iram. Waardoor dit keer op keer dezelfde variabelen zijn (soort van static). De reden hiervoor is om de stack niet te zwaar te belasten.

Simpele oplossingen hiervoor zijn:

 - Geen functies oproepen vanuit interrupt routines.
 - Hoofstuk 3.8 en 3.9 van de sdcc manual lezen.

