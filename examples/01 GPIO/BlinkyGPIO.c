//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>01 GPIO</h1>
//!
//! Een simpel voorbeeld programma om IO's in te lezen en te schrijven. <br>
//! Schakelaars 5678 gaan naar led_0123 <br>
//! Functietoetsen gaan naar led_4567 <br>
//!
//! \ref BlinkyGPIO.c
//
//*****************************************************************************
#include "xcez_lib.h"
#include "xc888.h" 

static const uint8_t LED_Data[] = {0b10000000,0b01000000,0b00100000,0b00010000,0b00001000,0b00000100,
                                   0b00000010,0b00000001,0b00000000,0b11111111,0b00000000,0b11111111};

void main (void){

	uint8_t i = 0;
	
	initleds();
	initftoetsen();
	initdipswitch();

	//startup led test door heel de poort aan te sturen:
	for(i=0; i < sizeof(LED_Data); i++){
		LEDS = ~ LED_Data[i]; //Leds branden bij een 0 dus bitpatroon inverteren
		delay(200);
	}
	
	
	while(1){
		//Functie toetsen testen in een if:
		if(F1 == IO_PRESSED){
			LED_7 = 0;	//LED AAN
		}else{
			LED_7 = 1;	//LED UIT
		}
		//Data kopieren:
		LED_6 = F2;
		LED_5 = F3;
		LED_4 = F4;
		//Een hele poort inlezen
		i = SWITCHES & 0x0F;		//Maar enkel de onderste nibbel behouden
		LEDS = (LEDS & 0xF0) | i;	//bij op de leds zetten
	}
}