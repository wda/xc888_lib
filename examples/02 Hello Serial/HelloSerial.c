//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>02 Hello Serial</h1>
//!
//! Een voorbeeld om via printf karakters naar de seriele poort te sturen. <br>
//! Kan handig zijn om te debuggen (voor te debuggen is UART beter dan LCD)
//!
//! \ref HelloSerial.c
//
//*****************************************************************************
#include <xc888.h>
#include <xcez_lib.h>
#include <stdio.h>
#include <stdlib.h> //needed for atoi

void main (void){
	uint8_t databyte = 0;
	char buffer[20];
	int getal;

	initsio();
	printf("Hello from serial at 9600 baud.\r\nPress a key: ");
	
	while(1){
		databyte = getchar();
		printf("You pressed %c\r\nEnter a your name: ",databyte);	
		gets(buffer);
		printf("Hello %s !\r\nNow enter a number: ", buffer);
		gets(buffer);
		getal = atoi(buffer);
		printf("The number you entered is %d.",getal);
	}
}