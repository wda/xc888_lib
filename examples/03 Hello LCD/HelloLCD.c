//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>03 Hello LCD</h1>
//!
//! Een voorbeeld karakters en commando's naar het LCD te sturen.
//!
//! \ref HelloLCD.c
//
//*****************************************************************************
#include "xcez_lib.h"
#include "xc888.h" 

void main (void){

	uint8_t i = 0;
	
	initlcd();
	
	//Een Karakter op het display zetten
	lcdputchar('A');
	lcdputchar('B');
	lcdputchar('C');
	//Een commando sturen kan op dezelfde manier:
	lcdputchar(LCD_NO_CURSOR); //Cursor onzichtbaar maken
	
	//Of de positie van de cursor veranderen:
	lcdputchar(LCD_SECONDLINE+(uint8_t)3); //2de lijn 4de karakter (0123 = 4de)
	lcdputchar('A');
	lcdputchar('B');
	lcdputchar('C');

	//Met lcdprintf kan je zinnen op het lcd krijgen. 
	//Eventueel met commando's bij:
	lcdprintf("%cDe waarde is xxx V.",LCD_THIRDLINE);
	
	while(1){
		//Merk op dat je het vorige altijd overschrijft
		//Als je eerst 12 schrijft en dan 2 krijg je 22
		//de 2 van de 12 is niet overschreven en de 1 wel
		lcdprintf("%c%3d",LCD_THIRDLINE+13,i++);
		delay(1000);	
	}
}