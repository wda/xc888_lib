//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>04 LCD  Custom Characters</h1>
//!
//! Een voorbeeld om de zelf karakters aan te maken voor het LCD (max 8).
//!
//! \ref CustomChar.c
//
//*****************************************************************************

#include "xcez_lib.h"
#include "xc888.h"

//Arrays met nieuwe karakter data
#include "newCharacters.h" 

void main (void){
	uint8_t i = 0;
	
	initlcd();
	
	lcdlight(1);

	while(1){	
		//Smily demo
		lcdprintf("%cCustom chars demo%c", LCD_CLEARSCREEN, LCD_NO_CURSOR);
		lcdbuild(newCharsSmile);	
		lcdputchar(LCD_SECONDLINE+(uint8_t)6);
	
		for(i=0; i<8; i++){
			lcdputchar(i);
		}

		lcdprintf("%cAnimatie: %cI%cU", LCD_THIRDLINE, LCD_FOURTHLINE+12, 0);
	
		for(i=0; i<20; i++){
			lcdprintf("%c%c", LCD_FOURTHLINE+13, i%2);
			delay(250);
		}
		//System Demo
		lcdbuild(newCharsSystem);
		lcdprintf("%cAnimatie: %c%c  ", LCD_THIRDLINE, LCD_FOURTHLINE+12, 0);
	
		for(i=0; i<25; i++){
			lcdprintf("%c%c", LCD_FOURTHLINE+12, i%4);
			delay(250);
		}

		//Symbols Demo
		lcdbuild(newCharsSymbols);
		lcdprintf("%cAnimatie: %c%c  ", LCD_THIRDLINE, LCD_FOURTHLINE+12, 0);
	
		for(i=0; i<20; i++){
			lcdprintf("%c%c %c", LCD_FOURTHLINE+12, i%2,i%2+6);
			delay(250);
		}

	}
}