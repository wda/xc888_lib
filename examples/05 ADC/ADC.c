//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>05 ADC</h1>
//!
//! Een voorbeeld om de potentiometer in te lezen 
//! en weer te geven op het scherm.
//! 
//! \ref ADC.c
//
//*****************************************************************************

#include "xcez_lib.h"
#include "xc888.h"

void main (void){
	uint16_t adcwaarde = 0;
	
	initlcd();
	initadcs();

	lcdprintf("%c%cPotmeter = 0x",LCD_NO_CURSOR, LCD_SECONDLINE+2);
	while(1){
		adcwaarde = adcpotmeter();
		lcdprintf("%c%03x",LCD_SECONDLINE+15,adcwaarde);
	}
}