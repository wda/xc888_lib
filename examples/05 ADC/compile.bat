REM Config: name the main application file (no extension):
SET CFILE=ADC

REM Compile:
sdcc -c --debug --use-stdout -V -I"..\\include" -I"..\\..\\include" --xram-loc 0xF000 --xram-size 0x600 --code-size 0x5000 %CFILE%.c

REM Compile lib:
sdcc -c --debug --use-stdout -V -I"..\\include" -I"..\\..\\include" --xram-loc 0xF000 --xram-size 0x600 --code-size 0x5000 ..\\..\\lib\\xcez_lib.c

REM Link everything:
sdcc --debug --use-stdout -V --xram-loc 0xF000 --xram-size 0x600 --code-size 0x5000 %CFILE%.rel xcez_lib.rel

REM even wachten:
pause

