//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>06 PWM</h1>
//!
//! Een voorbeeld om een PWM te genereren met p3.5, p3.3 en p3.1 als output
//!
//! De pwm is een 8 bit pwm die door t12 gemaakt wordt.
//! 
//! \ref PWM.c
//
//*****************************************************************************

#include "xcez_lib.h"
#include "xc888.h"

void main (void){

	uint8_t setVal= 0;	

	initleds();
	initadcs();
	initpwm();

	while(1){
		setVal = (uint8_t)(adcpotmeter() >> 2); //10bit meting maar ik heb er maar 8 nodig
		pwmset(0,setVal);
		pwmset(1,setVal + 0x55);
		pwmset(2,setVal + 0xAA);
		delay(50);
	}
}