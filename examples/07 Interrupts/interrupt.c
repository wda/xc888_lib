//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>07 Interrupts</h1>
//!
//! Een voorbeeld om met Timer0 interrupts te genereren 
//!
//! Timer0 is ingesteld in 8bit autoreload mode (mode2) 
//! en genereerd 48000 interrupts/s. <br>
//! Fpclk = 24MHz -> input van Timer0 = 12Mhz <br>
//! autoreload op 6 dus om de 250 clock pulsen interrupt <br>
//! 12Mhz/250 = 48000 interrupts/s <br>
//! Dit is nog te snel. Dus in de interrupt routine worden er 9599 interrupts 
//! "weg gegooid" => 48000 / 9600 = 5 keer een actie /s => 200ms
//!
//! De interrupt service routine inverteerd de waarde van LED_0
//! Het hoofdprogramma gerbruikt een software delay en inverteerd LED_1
//! 
//! Indien je de kans ziet moet je eens meten op P3.0 en P3.1 met een 
//! digitalescoop. Doe dezelfde meting ook met de tr0 bit op 0. 
//! - Wat doet dit met de software delay en waarom?
//! 
//! \ref interrupt.c
//
//*****************************************************************************

#include "xcez_lib.h"
#include "xc888.h"

void timer0_isr (void) __interrupt (TIMER0_INTERRUPT) 
{
	static uint16_t extraCounter = 9600;
	if(--extraCounter == 0){
		LED_0 =! LED_0;	
		extraCounter = 9600;
	}
}

void main (void){
	
	initleds();

	tmod = 0b00000010;	//Timer 0 8 bit autoreload timer geen gate-ing
	et0 = 1;			//Interrupts van Timer 0 toelaten
	ea = 1;				//Globale interrupt enable aanzetten

	//timing instellen
	th0 = 6;			//interrupt na 250 clock ticks (8bit = 256; 256 - 6 = 250)
	tl0 = 6;			//interrupt na 250 clock ticks (8bit = 256; 256 - 6 = 250)

	tr0 = 1;			//Timer 0 starten

	while(1){
		delay(200);
		LED_1 =! LED_1;
	}
}