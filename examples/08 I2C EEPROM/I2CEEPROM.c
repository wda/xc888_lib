//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>08 I2C EEPROM</h1>
//!
//! Een voorbeeld om een EEPROM op I2C te lezen en te schrijven
//! Dit zou de DDC2 interface van een VGA scherm kunnen zijn om de EDID 
//! data te kunnen lezen (name, resolution, ...)
//! 
//! \warning Het is niet aangewezen om naar een DDC2 te schrijven.<br> 
//! Het kan uw scherm onbruikbaar maken!!! 
//! 
//! \ref I2CEEPROM.c
//
//*****************************************************************************

#include "xcez_lib.h"
#include "xc888.h"

//! 2 van I2C
static const uint8_t newKarakter[] = {

    0b00001100,  
    0b00010010,
    0b00000100,
    0b00001000,
    0b00011110,
    0b00000000,
    0b00000000,
    0b10000000,	//EINDE    
};

static const uint8_t eepromWriteData[] = {
	1,2,3,4,5,6,7,8
};

//! \brief Code configureren om het LCD te gebruiken
//!
//! Omdat het LCD ook gebruikt maakt van de IIC bus kan het metingen 
//! op de bus, met bijvoorbeeld een Logic Analyser, onduidelijk maken.
#define USE_LCD 1
//! \brief Code configureren om data naar de eeprom te schrijven
//!
//! Omdat het LCD ook gebruikt maakt van de IIC bus kan het metingen 
//! op de bus, met bijvoorbeeld een Logic Analyser, onduidelijk maken.
#define USE_WRITE 1
//! \brief Code configureren het aantal te lezen bytes.
#define READ_LEN 16
//! \brief Code configureren output in hexadecimale vorm.
#define USE_HEX_OUTPUT 1

void main (void){
	uint8_t i = 0;
	uint8_t nack = 0;
	uint8_t databyte = 0;

	initsio();
	initleds();
	printf("\r\nI2C EEPROM demo\r\n");
	
	#if USE_LCD
	initlcd();
	lcdbuild(newKarakter);	
	lcdprintf("%C%cI%cC EEPROM Demo",LCD_NO_CURSOR, LCD_SECONDLINE+2, 0);
	#endif
		
	initiic();
	#if USE_WRITE
	printf("------schrijven------\r\n");
	for(i=0; i < sizeof(eepromWriteData); i++){
		do{	// Warning: Mogelijk een oneindige lus
			iicstart();		//vertrokken
			nack = iicoutbyte(0xA0); //I2C adres. schrijven naar I2C eeprom
			printf("%s op adres, ",nack?"Nack":"Ack");
		}while(nack);
		nack = iicoutbyte(i); //register adres waar we naar willen schrijven
		printf("%s op register adres %d, ",nack?"Nack":"Ack",i);
		nack = iicoutbyte(eepromWriteData[i]);  //data schrijven het bovenstaande adres
		printf("%s op data\r\n",nack?"Nack":"Ack");
		iicstop();      	//transactie sluiten	*/
	}
	#endif
	
	printf("\r\n------lezen------\r\n");
	iicstart();		//vertrokken
	nack = iicoutbyte(0xA0); //write om adres te kunnen schrijven 
	printf("%s op adres, ",nack?"Nack":"Ack");
	nack = iicoutbyte(0x00); //register adres van waar we willen lezen
	printf("%s op register adres %d, ",nack?"Nack":"Ack",0);
	iicstart();		//repeated start
	printf("repeated start, ");
	nack = iicoutbyte(0xA1); //lees adres 
	printf("%s op (lees)adres\r\n",nack?"Nack":"Ack");
	printf("\r\nNu zou de data moeten komen:\r\n");
	for(i=0; i < READ_LEN; i++){
		databyte = iicinbyte(IIC_ACK); 
		printf(USE_HEX_OUTPUT?"0x%x, ":"%d, ",databyte);
	}
	iicstop();      	//transactie sluiten
	printf("\r\nGedaan\r\n");
	
	
	while(1);
}