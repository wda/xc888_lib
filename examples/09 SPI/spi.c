//*****************************************************************************
//
//! \addtogroup Voorbeelden
//! <h1>09 SPI</h1>
//!
//! Een simpel voorbeeld om via SPI een byte naar buiten te sturen.
//! De SPI is ingesteld als een fullduplex master dus op hetzelfde moment dat
//! er een byte buiten gaat komt er ook een byte binnen.
//! De binnengelezen byte wordt op de Leds geplaatst.
//!
//! P1.2 is SCLK, P1.3 is MOSI, P1.4 is MISO, P1.5 is CS 
//! \ref spi.c
//
//*****************************************************************************

#include "xcez_lib.h"
#include "xc888.h"

void main (void){

	initleds();
		
	// De SPI instellen 
	// CPOL is 0, (clock is laag wanneer in rust)
	// CPHA is 0, (data samplen op de eerste klok puls)
	// 200Kbits/s,
	// MSB wordt eerst gestuurd.
	initspi(SPI_MODE00, SPI_MSB_FIRST, SPI_200K_BAUD);
	
	while(1){
		SPI_CS = 0;					//chip activeren
		LEDS = spioutbyte(0x0F);	//byte 0x07 naar buiten + inlezen ontvangen byte	
		SPI_CS = 1;					//chip deactiveren
		delay(2);
	}
}