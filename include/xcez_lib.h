#ifndef XCEZ_LIB_H
#define XCEZ_LIB_H

#include <xc888.h>
#include <stdint.h>
#include <stdio.h>
//! \addtogroup XC888_Lib XC888 Lib
//! @{
//! \addtogroup XC888_common XC888 Common
//! @{
	
//! \brief Onrechtstreeks nodig voor \b printf zijn output naar de UART te laten sturen
#define OUTPUT_TO_SERIAL 0
//! \brief Onrechtstreeks nodig voor \b printf zijn output naar het LCD te laten sturen
#define OUTPUT_TO_LCD 1
//! \brief Niet rechtstreeks aanpassen aub. Dit gebeurd automatisch via \b lcdprintf
extern uint8_t OutputMux;

#if __STDC_VERSION__ >= 201112L
char *gets(char *);
#endif	

//! @}
//! \addtogroup XC888_delay XC888 Delay
//! @{
	
void delay10us (void);
void delay1ms (void);
void delay (uint16_t ms);

//! @}
//! \addtogroup XC888_serieel XC888 Serial/UART
//! @{

void initsio (void);

//! @}
//! \addtogroup XC888_IIC XC888 IIC
//! @{

//! \brief Een "Acknowledge" op de IIC bus is laag. Helpt om de code leesbaarder te maken.
#define IIC_ACK 0
//! \brief Een "Not Acknowledge" op de IIC bus is hoog. Helpt om de code leesbaarder te maken.
#define IIC_NACK 1

void initiic (void);
void iicstart (void);
void iicstop (void);
uint8_t iicoutbyte (uint8_t databyte);
uint8_t iicinbyte (uint8_t ack);

//! @}
//! \addtogroup XC888_LCD XC888 LCD
//! @{
	
//! \brief Voor gebruik met \b out en \b outhnib. 
//! Normaal enkel intern gebruik. Geeft aan in welke mode de rs pin moet staan
#define LCD_COMMAND 	0		
//! \brief Voor gebruik met \b out en \b outhnib. 
//! Normaal enkel intern gebruik. Geeft aan in welke mode de rs pin moet staan
#define LCD_DATA		1
//! \brief Voor gebruik met \b lcdoutchar of \b lcdprintf. Scherm wissen.
#define LCD_CLEARSCREEN		16
//! \brief Voor gebruik met \b lcdoutchar of \b lcdprintf. Grote cursor aan (met blink).
#define LCD_BLOCK_BLINK 	17
//! \brief Voor gebruik met \b lcdoutchar of \b lcdprintf. Kleine cursor aan.
#define LCD_NO_BLOCK_BLINK	18
//! \brief Voor gebruik met \b lcdoutchar of \b lcdprintf. Geen cursor.
#define LCD_NO_CURSOR		19

//! \brief Voor gebruik met \b lcdoutchar of \b lcdprintf. Cursor op de eerste lijn (vooraan)
#define LCD_FIRSTLINE	(uint8_t)0x80
//! \brief Voor gebruik met \b lcdoutchar of \b lcdprintf. Cursor op de tweede lijn (vooraan)
#define LCD_SECONDLINE	(uint8_t)0xC0
//! \brief Voor gebruik met \b lcdoutchar of \b lcdprintf. Cursor op de derde lijn (vooraan)
#define LCD_THIRDLINE	(uint8_t)0x94
//! \brief Voor gebruik met \b lcdoutchar of \b lcdprintf. Cursor op de vierde lijn (vooraan)
#define LCD_FOURTHLINE	(uint8_t)0xD4

//! \brief Macro vermomd als functie. Via deze macro wordt printf gebruikt voor het LCD.
#define lcdprintf(s, v...) \
	OutputMux = OUTPUT_TO_LCD; \
	printf(s, ## v); \
	OutputMux = OUTPUT_TO_SERIAL;

//! \brief Normaal enkel intern gebruik. Om gemakelijk bits te kunnen aanpassen.
//! De buzzer is msb, rs is lsb, het geheel is uitleesbaar als byte.
typedef struct {
        union {
            struct {
                int rs:1;
                int e:1;
                int databits:4;
                int backlight:1;
                int buzzer:1;
            };
            uint8_t byte;
        };
} lcdPort_t;

void initlcd (void);
void out(uint8_t databyte, uint8_t data_or_not);
void lcdlight(uint8_t state);
void lcdbuzzer(uint8_t state);
void lcdputchar(uint8_t databyte);	
void lcdbuild (uint8_t *bitmapdata);

//! @}
//! \addtogroup XC888_GPIO XC888 GPIO
//! @{
	
//! \brief Alias voor p3_data zie \b initleds voor codevoorbeeld	
#define LEDS		p3_data
//! \brief Alias voor p3_data_0 zie \b initleds voor codevoorbeeld 
#define LED_0		p3_data_0
//! \brief Alias voor p3_data_1 zie \b initleds voor codevoorbeeld
#define LED_1		p3_data_1
//! \brief Alias voor p3_data_2 zie \b initleds voor codevoorbeeld
#define LED_2		p3_data_2
//! \brief Alias voor p3_data_3 zie \b initleds voor codevoorbeeld
#define LED_3		p3_data_3
//! \brief Alias voor p3_data_4 zie \b initleds voor codevoorbeeld
#define LED_4		p3_data_4
//! \brief Alias voor p3_data_5 zie \b initleds voor codevoorbeeld
#define LED_5		p3_data_5
//! \brief Alias voor p3_data_6 zie \b initleds voor codevoorbeeld
#define LED_6		p3_data_6
//! \brief Alias voor p3_data_7 zie \b initleds voor codevoorbeeld
#define LED_7		p3_data_7

//! \brief Alias voor p4_data zie \b initftoetsen voor gelijkaardig codevoorbeeld
#define SWITCHES	p4_data
//! \brief Alias voor p4_data_0 zie \b initdipswitch voor gelijkaardig codevoorbeeld
#define SWITCH_0		p4_data_0
//! \brief Alias voor p4_data_1 zie \b initdipswitch voor gelijkaardig codevoorbeeld
#define SWITCH_1		p4_data_1
//! \brief Alias voor p4_data_2 zie \b initdipswitch voor gelijkaardig codevoorbeeld
#define SWITCH_2		p4_data_2
//! \brief Alias voor p4_data_3 zie \b initdipswitch voor gelijkaardig codevoorbeeld
#define SWITCH_3		p4_data_3
//! \brief Alias voor p4_data_4 zie \b initdipswitch voor gelijkaardig codevoorbeeld
#define SWITCH_4		p4_data_4
//! \brief Alias voor p4_data_5 zie \b initdipswitch voor gelijkaardig codevoorbeeld
#define SWITCH_5		p4_data_5
//! \brief Alias voor p4_data_6 zie \b initdipswitch voor gelijkaardig codevoorbeeld
#define SWITCH_6		p4_data_6
//! \brief Alias voor p4_data_7 zie \b initdipswitch voor gelijkaardig codevoorbeeld
#define SWITCH_7		p4_data_7

//! \brief Alias voor p2_data_3 zie \b initdipswitch voor codevoorbeeld
#define F1			p2_data_3
//! \brief Alias voor p2_data_2 zie \b initdipswitch voor codevoorbeeld
#define F2			p2_data_2
//! \brief Alias voor p2_data_1 zie \b initdipswitch voor codevoorbeeld
#define F3			p2_data_1
//! \brief Alias voor p2_data_0 zie \b initdipswitch voor codevoorbeeld
#define F4			p2_data_0
//! \brief De Drukknoppen zijn actief laag. Deze define maakt de code leesbaarder.
//! zie \b initdipswitch voor codevoorbeeld
#define IO_PRESSED	0
//! \brief De Drukknoppen zijn actief laag. Deze define maakt de code leesbaarder.
//! zie \b initdipswitch voor codevoorbeeld
#define IO_NOT_PRESSED	1
void initleds (void);
void initftoetsen (void);
void initdipswitch (void);

//! @}
//! \addtogroup XC888_ADC XC888 ADC
//! @{

void initadcs (void);
uint16_t getadc (uint8_t channel);
uint16_t adcpotmeter(void);
uint16_t adclm335(void);

//! @}
//! \addtogroup XC888_PWM XC888 PWM
//! @{
void pwmset (uint8_t channel, uint8_t setValue);
void initpwm (void);

//! @}
//! \addtogroup XC888_SPI XC888 SPI
//! @{

//! \brief CPOL = 0, CPHA = 0
//!
//! CPOL bepaald de klok polariteit. Bij CPOL gelijk aan 0 is de klok laag in rust  <br>
//! CPHA bepaald de klok fase of m.a.w. bij welke flank er wat gebeurd <br>
//! bij CPHA gelijk aan 0 wordt de pin gelezen bij een stijgende flank en geschreven bij een dalende flank <br>
//! (eigenlijk wil CPHA = 0 zeggen sampelen op de eerste klok flank, CPHA = 1 sampelen op de tweede klok flank)
#define SPI_MODE00 0b00

//! \brief CPOL = 0, CPHA = 1
//!
//! CPOL bepaald de klok polariteit. Bij CPOL gelijk aan 0 is de klok laag in rust <br>
//! CPHA bepaald de klok fase of m.a.w. bij welke flank er wat gebeurd <br>
//! bij CPHA gelijk aan 1 wordt de pin gelezen bij een dalende flank en geschreven bij een stijgende flank <br>
//! (eigenlijk wil CPHA = 0 zeggen sampelen op de eerste klok flank, CPHA = 1 sampelen op de tweede klok flank)
#define SPI_MODE01 0b01

//! \brief CPOL = 1, CPHA = 0
//!
//! CPOL bepaald de klok polariteit. Bij CPOL gelijk aan 1 is de klok hoog in rust <br>
//! CPHA bepaald de klok fase of m.a.w. bij welke flank er wat gebeurd <br>
//! bij CPHA gelijk aan 0 wordt de pin gelezen bij een dalende flank en geschreven bij een stijgende flank <br>
//! (eigenlijk wil CPHA = 0 zeggen sampelen op de eerste klok flank, CPHA = 1 sampelen op de tweede klok flank)
#define SPI_MODE10 0b10

//! \brief CPOL = 1, CPHA = 1
//!
//! CPOL bepaald de klok polariteit. Bij CPOL gelijk aan 1 is de klok hoog in rust <br>
//! CPHA bepaald de klok fase of m.a.w. bij welke flank er wat gebeurd <br>
//! bij CPHA gelijk aan 1 wordt de pin gelezen bij een stijgende flank en geschreven bij een dalende flank <br>
//! (eigenlijk wil CPHA = 0 zeggen sampelen op de eerste klok flank, CPHA = 1 sampelen op de tweede klok flank)
#define SPI_MODE11 0b11

//! \brief LSB eerst sturen (te gebruiken bij initspi)
#define SPI_LSB_FIRST	0b0
//! \brief MSB eerst sturen (te gebruiken bij initspi)
#define SPI_MSB_FIRST	0b1

//! \brief 12 Mbits/s (te gebruiken bij initspi)
#define SPI_12M_BAUD	0x00
//! \brief 6 Mbits/s (te gebruiken bij initspi)
#define SPI_6M_BAUD		0x01
//! \brief 1,3M bits/s (te gebruiken bij initspi)
#define SPI_1M3_BAUD	0x08
//! \brief 1M bits/s (te gebruiken bij initspi)
#define SPI_1M_BAUD		0x0B
//! \brief 750K bits/s (te gebruiken bij initspi)
#define SPI_750K_BAUD	0x0F
//! \brief 666,7 Kbits/s (te gebruiken bij initspi)
#define SPI_666K7_BAUD	0x11
//! \brief 600 Kbits/s (te gebruiken bij initspi)
#define SPI_600K_BAUD	0x13
//! \brief 500 Kbits/s (te gebruiken bij initspi)
#define SPI_500K_BAUD	0x17
//! \brief 266,7 Kbits/s (te gebruiken bij initspi)
#define SPI_266K7_BAUD	0x2C
//! \brief 200 Kbits/s (te gebruiken bij initspi)
#define SPI_200K_BAUD	0x3B
//! \brief 133,3 Kbits/s (te gebruiken bij initspi)
#define SPI_133K3_BAUD	0x59
//! \brief 100 Kbits/s (te gebruiken bij initspi)
#define SPI_100K_BAUD	0x77

//! \brief Chip select pin actief laag. 
//! 
//! Te gebruiken als:
//! \code
//! SPI_CS = 0;		//chip activeren
//! spioutbyte(data);
//! SPI_CS = 1;		//chip deactiveren
//! \endcode
#define SPI_CS p1_data_5

void initspi (uint8_t mode, uint8_t heading, uint8_t speed);
uint8_t spioutbyte(uint8_t dataByte);


//! @}
//! \addtogroup XC888_INT XC888 Interrupts
//! @{
// Interrupt vector sources from highest to lowest priority
// (vectoradres - 3) / 8 = SDCC getal voor interrupt
#define NMI                         14 //0x0073
#define EXTERNAL_INTERRUPT0          0 //0x0003                         
#define TIMER0_INTERRUPT             1 //0x000B                        
#define EXTERNAL_INTERRUPT1          2 //0x0013                         
#define TIMER1_INTERRUPT             3 //0x001B                          
#define UART_INTERRUPT               4 //0x0023                         
#define TIMER2_INTERRUPT             5 //0x002B
#define ADC_INTERRUPT                6 //0x0033                
#define SSC_INTERRUPT                7 //0x003B                          
#define EXTERNAL_INTERRUPT2          8 //0x0043
#define CCU6_INTERRUPT_NODE0        10 //0x0053
#define CCU6_INTERRUPT_NODE1        11 //0x005B
#define CCU6_INTERRUPT_NODE2        12 //0x0063
#define CCU6_INTERRUPT_NODE3        13 //0x006B
//! @}
//! @}
#endif /* XCEZ_LIB_H */

	

	


	

