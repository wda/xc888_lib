//*************************************************************************************************
//
//!	\brief De eigenlijke code van de XC888 lib 
//! 
//! \file xcez_lib.c
//! \author Dams Wim
//! \date 29 jan 2016
//! 
//! \addtogroup XC888_common XC888 Common
//! @{
//! \brief Dit deel bevat het gemeenschappelijke deel van de library.
//!
//! Zo kan bijvoorbeeld putchar zowel voor de uart als voor het LCD gebruikt worden.
//
//*************************************************************************************************
#include <xcez_lib.h>

//! Variabele die bepaald waar printf zijn output naar stuurt
uint8_t OutputMux = OUTPUT_TO_SERIAL;	
//! Variabele om gemakkelijk de juiste bits te manipuleren (opgepast soms bug bij sdcc2.9)
static lcdPort_t lcdport;	

//*************************************************************************************************
//
//! \brief eigen implementatie van de gets . 
//!
//! deze functie zit niet meer in SDCC vanaf 3.8.0
//! 
//! \note De conditional compile zou moeten overeenkomen met de stdio.h van SDCC
//
//*************************************************************************************************

#if __STDC_VERSION__ >= 201112L
char *gets(char * buffer){
	uint16_t teller = 0;
	for(uint16_t i=0; i < UINT16_MAX; i++){ 
		buffer[i] = getchar();
		if(buffer[i] == '\n'){
			buffer[i] = '\0';
			return buffer;
		}
	}
	return buffer;
}
#endif						

//*************************************************************************************************
//
//! \brief zal een karakter naar de output sturen. 
//!
//! De output kan het LCD of de UART zijn. Dit is afhankelijk van de globale variabele 
//! OutputMux. Zie ook de defines \b OUTPUT_TO_SERIAL en \b OUTPUT_TO_LCD.
//! 
//! param c Het karakter dat naar de output gestuurd moet worden. 
//! Indien de output de LCD is kan het ook een commando zijn zie \b lcdputchar.
//! 
//! \note Deze functie wordt opgeroepen door printf. 
//! Je kan ze ook rechtstreeks gebruiken.
//
//*************************************************************************************************
int putchar(int c) {
	if (OutputMux == OUTPUT_TO_SERIAL){
		ti=0;	
		sbuf=c;
		while(!ti); //wachten tot het karakter is verzonden
	}else{
		lcdputchar(c);
	}
	return c;
}

//*************************************************************************************************
//
//! \brief zal een karakter lezen van de UART
//! 
//! \note Dit is een blocking call! 
//! M.a.w. de controller wacht tot er effectief een karakter is ontvangen.
//! Indien dit niet gewenst is kan je hetvolgende doen:
//!
//! \code 
//! if (ri){	//kijken of er data is
//! 	var = getchar(); //data inlezen
//! } 
//! \endcode
//
//*************************************************************************************************
int getchar(void) {	
	while(!ri); //wachten tot het karakter is ontvangen
	ri=0;
	return sbuf;
}

//! @}
//! \addtogroup XC888_delay XC888 Delay
//! @{
//! \brief functies om actief een tijdje te wachten
//!
//! De timing van deze functies wordt afgepast aan de hand van instructie tijden. En is dus
//! verre van perfect. Daar bovenop zijn ze onderhevig aan veranderingen van de clock 
//! en duren ze langer als ze onderbroken worden door interrupts.
	
//*************************************************************************************************
//
//! \brief Tijdsvertraging van 10us. 
//!
//! Wordt gebruikt om de IIC bus te vertragen tot een maximale snelheid van 100kbit/s 
//! In realiteit zal de bussnelheid lager liggen. 
//! We houden immers geen rekening met de uitvoeringstijd van de routines,
//! en de gebruikte delay is dubbel zo lang als stikt nodig (5us volstaat).
//
//*************************************************************************************************

void delay10us (void){

__asm
	push   acc                  //;8*42ns=330ns
	push   psw                  //;8*42ns=330ns
	mov    acc,#0x1a            //;420ns
0002$:	
	djnz   acc,0002$                //;420ns*aantal keer doorlopen
	pop    psw                  //;8*42ns=330ns
	pop    acc                  //;8*42ns=330ns
__endasm;
	
}				//ret ;165ns


//*************************************************************************************************
//
//! \brief Tijdsvertraging van 1 ms.
//
//*************************************************************************************************

void delay1ms (void){
	uint8_t i;
	for(i=0; i < 100; i++){
		delay10us();
	}	
}	

//*************************************************************************************************
//
//! \brief Is een tijdsvertraging van een aantal milliseconden.
//!
//! \note Deze tijdvertraging is niet nauwkeurig. 
//! \param ms het aantal ms dat er moet gewacht worden (0-65535)
//
//*************************************************************************************************

void delay (uint16_t ms){

	uint16_t i=0;
	
	for(i=0;i < ms;i++){
		delay1ms();
	}
}

//! @}
//! \addtogroup XC888_serieel XC888 Serial/UART
//! @{
//! \brief Functies om de UART te gebruiken.
//! 
//! Om data via de UART te zenden kan je \b printf of \b putchar gebruiken <br>
//! Om data te ontvangen \b getchar of \b scanf.

//*************************************************************************************************
//
//! \brief Zet de seriele poort klaar  voor gebruik. De baudrate is 9600.
//
//*************************************************************************************************

void initsio (void){
	uint8_t tmpStack[3];
	//3 registers bewaren om daarna terug te zetten
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	tmpStack[2] = scu_page;
	
	syscon0 = 0x04;  //hier zit al wat we nodig hebben
	scu_page = 0x00; //pagina 0 system control regs
	
	port_page = 0x02; //pagina 2 selecteren
	p1_altsel0 &= 0b11111101; 
	p1_altsel0 |= 0b00000001;
	p1_altsel1 &= 0b11111110;
	p1_altsel1 |= 0b00000010;

	port_page = 0x00; //pagina 0 selecteren
        p1_dir &= 0b11111110; //p1.0=input
        p1_dir |= 0b00000010; //p1.1=output

	// Nu moet de baud rate generator ingesteld worden
	// We gaan er van uit dat de systeemklok 24MHz is

        scon = 0b01010000;     //UART initialiseren

	// LET OP!!!!!!!!!!!!!! eerst BG laden, dan bcon, anders wordt BG waarde niet gebruikt!!

        bg = 155; //zie 12-13 in XC888 UM
	bcon = 0b00010001;
	
	syscon0 = tmpStack[0];
	port_page = tmpStack[1];
	scu_page = tmpStack[2]; 
}

//! @}
//! \addtogroup XC888_IIC XC888 IIC
//! @{
//! \brief functies voor een software IIC.
//!
//! Deze functie worden o.a. door het LCD gebruikt.
//! De IIC pinnen komen ook op een vlaggestekker onderaan het bord en op de LM75 temp. sensor	
	
#define sda		p0_data_7            //!< pin gebruikt als sda	
#define scl     p0_data_3            //!< pin gebruikt als scl
// no doxygen output please
//! \cond 
#define sdaout  0b10000000           //!< masker om sda als output te schakelen
#define sdain   0b01111111           //!< masker om sda als input te schakelen
//! \endcond 

//*************************************************************************************************
// 
//! \brief Initialisatie van de software IIC interface
//!
//! Deze subroutine die de pinnen sda en scl (zie define) klaar zet
//! voor gebruik als IIC bus. De andere poortpinnen worden niet aangepast
//! scl wordt als oc output geschakeld met data waarde 1 (bus in rust)
//! sda wordt als input geschakeld
//!
//! \note LET OP!!
//! Dit is een IIC bus met beperkte mogelijkheden:
//! - vaste communicatiesnleheid van 100kbit/s
//! - enkel master mode
//! - enkel 7 bit adressering
//! - geen klok stretching
//
//*************************************************************************************************

void initiic (void)
{
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	
	syscon0 = 0x04;		//juiste map selecteren
	port_page = 0x00;	//pagina 0 selecteren	
	sda = 1;		//data op 1 zetten (ruststand iic)
	scl = 1;		//klok op 1 zetten (ruststand iic)
	port_page = 0x03;	//p0_od toegankelijk maken
	scl = 1;		//klok werkt als od
	sda = 1;		//sda werkt als od
	
	port_page = 0x00;	//terug pagina 0 selecteren
 	p0_dir |= 0b00001000;	//scl als output selecteren
  	p0_dir &= 0b01111111;	//sda als input selecteren
	
	syscon0 = tmpStack[0];
	port_page = tmpStack[1];
}

//*************************************************************************************************
//
//! \brief Wekt een startconditie op.
//!
//! Bij de start conditie wordt eerst de scl laag gemaakt, dan de sda. Om de bittijd
//! te garanderen volgt er een delay van 10us
//
//*************************************************************************************************

void iicstart (void){
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	
	syscon0 = 0x04;		//hoofdmap selecteren
        port_page = 0x00;      	//hier zit alles wat we nodig hebben
        scl = 1;		//zeker zijn dat we vanuit ruststand vertrekken
        sda = 1;		//idem
        p0_dir |= sdaout;        //pin als output schakelen
        delay10us();
        sda = 0;		//data laag
        delay10us();
        scl = 0;		//dan klok laag
        delay10us();		//bit lang genoeg maken
	
	syscon0 = tmpStack[0];
	port_page = tmpStack[1];
}

//*************************************************************************************************
//
//! \brief Wekt een stopconditie op.
//!
//! Bij de stopconditie wordt eerst de scl laag gemaakt, dan de sda hoog. 
//! Om de bittijd te garanderen volgt er een delay van 10us
//
//*************************************************************************************************

void iicstop (void){
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	
	syscon0 = 0x04;		//hoofdmap selecteren
        port_page = 0x00;      	//hier zit alles wat we nodig hebben
        p0_dir |= sdaout;        //pin als output schakelen
        sda = 0;		//data naar omlaag
        delay10us();
        scl = 1;		//klok naar 1
        delay10us();		//wachten
        sda = 1;		//idem data
        delay10us();
	p0_dir &= sdain;	//pin terug als input schakelen.
	
 	syscon0 = tmpStack[0];
	port_page = tmpStack[1];
}

//*************************************************************************************************
//
//! \brief Stuurt de data naar buiten stuurt op de iic bus.
//!
//! In totaal worden er 8 bits verzonden. Daarna wordt een negende klokpuls
//! gegenereerd om de ack bit in te lezen. Die wordt teruggegeven zie return
//!
//! @param databyte de te verzenden byte
//! @return ack bit vanuit de slave
//
//*************************************************************************************************

uint8_t iicoutbyte (uint8_t databyte){
	uint8_t i=0;
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	
	syscon0 = 0x04;		//hoofdmap selecteren
        port_page = 0x00;      	//hier zit alles wat we nodig hebben
        
        p0_dir |= sdaout;	//poortpin als output zetten
        
        for(i=0; i < 8; i++){	//hier gaan we dan (bit per bit naar buiten)
        	sda = (databyte & 0x80);	//op pin
        	delay10us();	//wachten
        	scl = 1;	//puls genereren op klok pin
        	delay10us();	//klok terug laag maken
        	scl = 0;	//puls genereren op klok pin
        	databyte = databyte << 1;
        }

	// de 8 bits zijn weg, nu de ack binnen nemen.
	
	p0_dir &= sdain;	//pin als input schakelen
	delay10us();		//wachten tot data zeker beschikbaar
	databyte = 0x00;
	scl = 1;		//puls terug hoog
	databyte = sda;         //inlezen 
        delay10us();
        scl = 0;		//klok blijft laag achter
        syscon0 = tmpStack[0];
	port_page = tmpStack[1];
	return databyte;	//ack of niet terug geven
}

//*************************************************************************************************
//
//! \brief Leest een byte in en genereerd zelf een nack of ack op de IIC bus. 
//!
//! Bij het inlezen van een byte vanop een IIC bus moet elke byte die 
//! gelezen wordt ge-acked worden behalve de laatste. 
//! \param ack Het \e ack argument moet 1 van volgende waardes zijn:
//! \b IIC_ACK, \b IIC_NACK.
//! \return De ontvangen byte
//
//*************************************************************************************************
 
uint8_t iicinbyte (uint8_t ack){
	uint8_t i=0;
	uint8_t databyte = 0;
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	
	syscon0 = 0x04;		//hoofdmap selecteren
        port_page = 0x00;      	//hier zit alles wat we nodig hebben
	p0_dir &= sdain;	//pin als input schakelen
	
	for(i=0; i < 8; i++){
		delay10us();
		scl = 1;	//klok hoog
		databyte = databyte << 1;
              	databyte |= sda;//pin inlezen
		delay10us();
		scl = 0;	//klok terug laag
	}
        p0_dir |= sdaout;	//pin als output schakelen
        sda = ack;              //ack of nack genereren
        delay10us();
        scl = 1;
        delay10us();
        scl = 0;
        p0_dir &= sdain;	//pin terug als input schakelen
        syscon0 = tmpStack[0];
	port_page = tmpStack[1];
	return databyte;	//ack of niet terug geven
}

//! @}
//! \addtogroup XC888_LCD XC888 LCD
//! @{
//! \brief Functies voor het LCD aan te sturen.	

//*************************************************************************************************
//
//! \brief Het LCD wordt aangestuurd via een IO expander. Deze routine stuurt deze IO expander aan
//!
//! \param databyte De waarde die naar de IO expander moet worden gestuurd
//! \note Dit is een interne functie. 
//
//*************************************************************************************************
 
static void porttolcd (uint8_t databyte){
	iicstart();		//vertrokken
	iicoutbyte(0b01000000); //schrijven naar expander
	iicoutbyte(databyte); 
	iicstop();      	//transactie sluiten
}

//*************************************************************************************************
//
//! \brief Het LCD wordt aangestuurd via een IO expander. Deze routine leest deze IO expander uit.
//!
//! Op die manier kunnen we de expander laten onthouden wat de waarde van de buzzer en backlight is.
//! \return de waarde van de IO expander
//! \note Dit is een interne functie. 
//
//*************************************************************************************************
 
static uint8_t lcdtoport (void){
	uint8_t retv;
	iicstart();		//vertrokken
	iicoutbyte(0b01000001); //lezen van expander
	retv = iicinbyte(IIC_NACK); //data inlezen
	iicstop();      	//transactie sluiten
	return retv;
}

//*************************************************************************************************
//
//! \brief outhnib stuurt de hoge 4 bits van \b databyte naar het LCD scherm.
//! 
//! \param data_or_not Bepaald of het een commando is of data (karakters) en moet 1 van volgende 
//! waardes zijn: \b LCD_COMMAND, \b LCD_DATA hierdoor weet de functie wat er met de rs 
//! pin van het LCD moet gebeuren.
//! \note Dit is een interne functie. 
//
//*************************************************************************************************

static void outhnib (uint8_t databyte, uint8_t data_or_not){
	//lcdPort_t lcdport;
	lcdport.byte = lcdtoport();	//lees de expander
	lcdport.rs = data_or_not;		//controle lijn in orde voor data of commando
	lcdport.e = 0;			//deselectie display
	lcdport.databits = (databyte & 0xf0)>>4;
	porttolcd(lcdport.byte);
	lcdport.e = 1;
	porttolcd(lcdport.byte);
	lcdport.e = 0;
	porttolcd(lcdport.byte);
}

//*************************************************************************************************
//
//! \brief deze routine stuurt 1 volledige byte naar het LCD via de 4 bit interface 
//!
//! \note gebruik deze functie alleen als het echt nodig is. 
//! In de meeste gevallen is \b lcdputchar beter geschikt
//! 
//!	\param databyte De byte die naar het display gestuurd word.
//!	\param data_or_not Bepaald of het een commando is of data (karakters)
//!	en moet 1 van volgende waardes zijn: \b LCD_COMMAND, \b LCD_DATA hierdoor  
//! weet de functie wat er met de rs pin van het LCD moet gebeuren.
//
//*************************************************************************************************

void out(uint8_t databyte, uint8_t data_or_not){
	outhnib(databyte, data_or_not);     //hoogste vier bits verzenden
        delay(2);		//niet echt nodig
        outhnib(databyte<<4, data_or_not);  //laagste vier bits verzenden
        delay(2);		//altijd goed voor traagste commando
}

//*************************************************************************************************
//
//! \brief Deze subroutine kan de backlight in of uitschakelen
//! 
//!	\param state \b 1 = backlight ingeschakeld, \b 0 = backlight uitgeschakeld
//
//*************************************************************************************************

void lcdlight(uint8_t state){
	//! \todo Door een bug (in sdcc?) lijkt de onderstaande code niet te werkten
	//! De code doet in principe een read modify write. Alleen bij de write schrijft 
	//! hij de orginele waarde ipv de aangepaste. Deze code heeft gewerkt op met
	//! een andere versie van sdcc. Dus nakijken of dit bij sdcc 3 ook is en of 
	//! dit gaat met Silicon Labs IDE
	//!
	//! \code
	//! lcdPort_t lcdport;
	//! lcdport.byte = lcdtoport();	//expander lezen
	//! lcdport.backlight = !state;	//backlight is aan bij 0!
	//! porttolcd(lcdport.byte);	//expander schrijven 
	//! \endcode
	if(state){
		state = lcdtoport();
		state &= 0b10111111;
	}else{
		state = lcdtoport();
		state |= 0b01000000;
	}
	porttolcd(state);
}

//*************************************************************************************************
//
//! \brief Deze subroutine kan de buzzer in of uitschakelen
//! 
//!	\param state \b 1 = buzzer ingeschakeld, \b 0 = buzzer uitgeschakeld
//! 
//! \note de buzzer is standaard niet aangesloten op het bord. (zie JP6 op pcb en schema)
//
//*************************************************************************************************

void lcdbuzzer(uint8_t state){
	//! \todo zelfde todo als voor lcdlight
	if(state){
		state = lcdtoport();
		state &= 0b01111111;
	}else{
		state = lcdtoport();
		state |= 0b10000000;
	}
	porttolcd(state);
}

// no doxygen output please
//! \cond 
#define cleardisp     0x01                 //!< Leeg maken van het scherm
#define cursathom     0x02                 //!< Cursor op de eerste plaats zetten
#define entrymode     0b00000110            //!< Cursor naar rechts, scherm vast
#define displayon     0b00000001            //!< Homen en clear cursor
#define displayof     0b00001000            //!< Display off, cursor off
#define cursoroff     0b00001100            //!< Display on zonder cursor
#define cursoronb     0b00001111            //!< Cursor on and blink
#define cursoronn     0b00001110            //!< Cursor on no blink
#define functions     0b00101000            //!< Interface lengte en karakter font
#define cgram         0b01000000            //!< Selectie karakter generator ram
#define ddram         0b10000000            //!< Selectie data display ram
//! \endcond

//*************************************************************************************************
//
//! \brief zal het scherm resetten en initialiseren voor gebruik via een 4 bit bus.
//!  
//! De routine zet het scherm klaar voor normaal gebruik: ingave van
//! links naar rechts zonder display shift, normale karakterfont. De cursor staat
//! aan als een knipperend lijntje onderaan de regel. Het scherm is leeg.
//
//*************************************************************************************************

void initlcd (void){
	initiic();				//vermits de LCD gebruikt wordt via een IIC bus
	porttolcd(0b11111101);	//poort in uitgangstoestand zetten buzzer en baklight worden nu op 1 gezet
	delay(32);				//bij opstarten meer dan 30ms wachten
	
	outhnib(0b00110000, LCD_COMMAND);	//volgende drie schrijfbeurten zijn een truuk om het display altijd opgestart te krijgen.
	delay(2);
	outhnib(0b00110000, LCD_COMMAND);	
	delay(2);
	outhnib(0b00110000, LCD_COMMAND);	
	delay(2);
        
	outhnib(0b00100000, LCD_COMMAND);	//instellen 4 bit interface gebruiken
    delay(2);							//niet nodig volgens datasheet

	out(functions, LCD_COMMAND);	//4 bit interface, twee lijnen 5*8 dots
	delay(2);
	
	out(cursoronb, LCD_COMMAND);	//display on cursor on and blink
	delay(2);
	
	out(displayon, LCD_COMMAND);	//clear display and home cursor
	delay(2);
	
	out(entrymode, LCD_COMMAND);	//display klaar zetten normaal gebruik
	delay(2);						//wachten weer veel te lang
	
}

//*************************************************************************************************
//
//! \brief deze functie zal de ascii code of het commando naar het lcd scherm sturen. 
//!
//! \param databyte 
//! - De codes groter of gelijk aan 0x80 zijn een adres en dus een commando
//! zie \b LCD_FIRSTLINE, \b LCD_SECONDLINE, \b LCD_THIRDLINE, \b LCD_FOURTHLINE
//! - Ook de volgende codes zijn commando's: \b LCD_CLEARSCREEN, \b LCD_BLOCK_BLINK, 
//! \b LCD_NO_BLOCK_BLINK, \b LCD_NO_CURSOR
//! - De codes kleiner dan 8 zijn de custom karakters (zie ook \b lcdbuild)
//! - De codes kleiner dan 0x80 en groter dan 20 zijn ASCII karakters
//!
//! \code
//! lcdputchar('a'); // Het karakter a op het lcd zetten
//! lcdputchar(LCD_SECONDLINE + 6); // De cursor op de 2de lijn 7de plaats zetten
//! lcdputchar(0);	// Eerste custom karakter op LCD plaatsen (eerst lcdbuild oproepen)
//! lcdputchar(LCD_NO_CURSOR); // Cursor afzetten
//! \endcode
//
//*************************************************************************************************

void lcdputchar(uint8_t databyte){
	if(databyte >= 0x80){
		//Het is een adres
		out(databyte, LCD_COMMAND);
		return;
	}
	if(databyte > 20 || databyte < 8){
		//het is een karakter
		out(databyte, LCD_DATA);
		return;
	}

	//het is een commando
	if(databyte == LCD_CLEARSCREEN){	//scherm wissen en cursor in home position
		out(cleardisp, LCD_COMMAND);
		out(cursathom, LCD_COMMAND);
		return;	
	}
	if(databyte == LCD_BLOCK_BLINK){	//geen cursor on blink
		out(cursoronb, LCD_COMMAND);
		return;	
	}
	if(databyte == LCD_NO_BLOCK_BLINK){	//geen cursor on no blink
		out(cursoronn, LCD_COMMAND);
		return;	
	}
	if(databyte == LCD_NO_CURSOR){		//cursor uitschakelen
		out(cursoroff, LCD_COMMAND);
		return;	
	}
}

//*************************************************************************************************
//
//! \brief voor zelf karakters samen te stellen
//! 
//! De routine zal de interne karakatergenerator van het LCD laden met het 
//! een bitpatroon voor maximum 8 karakters.
//! 
//! \param bitmapdata Is een array van uint8_t data (1 = zwarte pixel)
//! enkel de eerste bits [6-0] worden gebruikt voor het karakter.
//! Als bit 7 op 1 staat stopt de routine met het laden van de data.
//! maw zet een 1 op bit 7 van de laatste byte van het laatste karakter.
//! De nieuwe karakters zijn niet zichtbaar gebruik \b lcdputchar(x); 
//! om het nieuwe karakter op het scherm te plaatsen waar x een getal is tussen 0 en 7
//! 
//! \code
//! // array met bitmap data
//! const uint8_t newCharSmile[] = {               
//!    0b00000000,   // :-)
//!    0b00001010,
//!    0b00000000,
//!    0b00000100,
//!    0b00000000,
//!    0b00010001,
//!    0b00001110,
//!    0b10000000  // EINDE want bit 7 is 1
//! };
//! lcdbuild(newCharSmile);	// het karakter bouwen
//! lcdputchar(0);			// het karakter op het scherm plaatsen.
//! \endcode
//
//*************************************************************************************************
 
void lcdbuild (uint8_t *bitmapdata){
	out(cgram, LCD_COMMAND);			//Karakter generator RAM selecteren
	do{
		out(*bitmapdata, LCD_DATA);
	}while(((*(bitmapdata++)) & 0x80) != 0x80);
	out(ddram, LCD_COMMAND);			//Data display RAM selecteren
}

//! @}
//! \addtogroup XC888_GPIO XC888 GPIO
//! @{
//! \brief Functies voor de GPIO te configureren.

//*************************************************************************************************
//
//! \brief Is een routine die poort 3 als output configureerd.
//! 
//! De LED's worden ook gedoofd.
//!
//! \note Om een led te laten branden moet je een \b 0 sturen. Ze zijn actief laag
//!
//! \code
//! LEDS = 0x55; //msb aan,uit,aan,uit,aan,uit,aan,uit lsb
//! LEDS = 0xAA; //msb uit,aan,uit,aan,uit,aan,uit,aan lsb
//! LED_0 = 1;   //eerste led (rechts) uit
//! \endcode
//
//*************************************************************************************************

void initleds (void){
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	
	syscon0 = 0x04;  //juiste map selecteren	
        port_page = 0x00; //selecteer poort page 0
        p3_dir = 0xff;    //poort 3 als output schakelen

 	syscon0 = tmpStack[0];
	port_page = tmpStack[1];
}

//*************************************************************************************************
//
//! \brief is een routine die de 4 functieschakelaars onder het LCD display
//! klaar zet voor gebruik met een schakelaar naar massa. 
//!
//! De overige 4 pinnen van poort 2 behouden hun originele instelling.
//! 
//! \note als er op de knop gedrukt wordt krijg je een 0 binnen. Ze zijn actief laag.
//! Voor het gemak en leesbaarheid van de code kan je \b IO_PRESSED of \b IO_NOT_PRESSED gebruiken
//!
//! \code
//! if(F1 == IO_PRESSED){
//! 	// Reageer op de knop
//! 	... 
//! }
//! \endcode
//
//*************************************************************************************************

void initftoetsen (void){
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	syscon0 = 0x04;			//juiste map selecteren
	port_page = 0x01;		//selecteer poort page 1
        p2_pudsel |= 0x0f;		//selecteer pull_up device (onderste 4 pinnen)
 	p2_puden |= 0x0f;		//selectie inschakelen (onderste 4 pinnen)
	port_page = 0x00;		//pagina 0 selecteren
	p2_dir &= 0xf0;			//input drivers activeren (onderste 4 pinnen)
 	syscon0 = tmpStack[0];
	port_page = tmpStack[1];
}

//*************************************************************************************************
//
//! \brief is een subroutine die de pinnen van poort 4 insteld als input met
//! pull-up weerstanden ingeschakeld. 
//!
//! Hierdoor kunnen schakelaars gebruikt worden naar massa zonder extra weerstanden.
//! 
//! \note als de knop in de ON positie staat leest de controller ze in als een 0. Ze zijn actief laag.
//!
//! \code
//! LEDS = SWITCHES;
//! \endcode
//
//*************************************************************************************************

void initdipswitch (void){
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	syscon0 = 0x04;			//juiste map selecteren
	port_page = 0x01;		//selecteer poort page 1
	p4_pudsel = 0xff;		//selecteer pull_up device
	p4_puden = 0xff;		//selectie inschakelen
	port_page = 0x00;		//pagina 0 selecteren
        p4_dir = 0x00;			//poort 4 als input schakelen
 	syscon0 = tmpStack[0];
	port_page = tmpStack[1];
}

//! @}
//! \addtogroup XC888_ADC XC888 ADC
//! @{
//! \brief Functies voor de ADC in te stellen en te gebruiken

//*************************************************************************************************
//
//! \brief Zal de adc klaar zetten voor gebruik met de sequentiele mode van de ADC.
//! 
//! Door de sequentiele mode te gebruiken kunnen alle analoge ingangen gebruikt worden.
//
//*************************************************************************************************

void initadcs (void){
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = adc_page;
	syscon0 = 0x04;				//basis pagina selecteren
	adc_page = 0x00;			//pagina 0 selecteren
	adc_globctr = 0b10010000;	//adc inschakelen                   MUST!!
								//klok delen door 3 (8MHz) Voldoetr aan de vereiste dat adcklok<10MHZ
								//Sneller werkt ook (deler door 2) 10 bit uitkomst
	adc_inpcr0 = 0x00;			//sample time verlengen met x klokpulsen (nu op 0)
//Dit is niet echt nodig, maar hiermee vangen we de hoge impedantie een input op door de waarde groter dan 0 te nemen
	adc_prar = 0b01000000;		//arbitration slot seri�le arbiratie activeren MUST!!
	adc_page = 0x06;			//adc page 6 selecteren             MUST!!   
	adc_qmr0 = 0b00000001;		//engt=1 source on                  MUST!!
	syscon0 = tmpStack[0];
	adc_page = tmpStack[1];
}

//*************************************************************************************************
//
//! \brief Meet de spanning (10bit) op 1 van de 8 kanalen (p2.0-p2.7)
//!
//! \param channel Het kanaalnummer waarop de meting gebeurd
//!	- 0 =kanaal p2.0 header H2 klem 1
//!	- 1 =kanaal p2.1 header H2 klem 2
//!	- 2 =kanaal p2.2 header H2 klem 3
//!	- 3 =kanaal p2.3 header H2 klem 4
//!	- 4 =kanaal p2.4 header H2 klem 5 (potmeter, remove JP7 for H2 input)
//!	- 5 =kanaal p2.5 header H2 klem 6 (LM335   , remove JP8 for H2 input)
//!	- 6 =kanaal p2.6 header H2 klem 7
//!	- 7 =kanaal p2.7 header H2 klem 8
//!
//! \return:
//! 	10 bit meting in een uint16_t
//
//*************************************************************************************************

uint16_t getadc (uint8_t channel){
	uint16_t resultaat;
	uint8_t tmpStack[2];
	tmpStack[0] = syscon0;
	tmpStack[1] = adc_page;
	syscon0 = 0x04;				//map kiezen
	adc_page = 0x06;			//adc pagina 6 kiezen
	adc_qinr0 = channel;		//kanaal laden (fout in adc defs xcez1 bestand)
	adc_page = 0x00;            //naar pagina 0 schakelen
__asm							//anders loopt het mis met het testen van de ready bit
	nop
	nop					
__endasm;
	while(adc_globstr & 0x01);	//wachten to ADC klaar is
	adc_page = 0x02;			//pagina 2 selecteren
    resultaat = adc_resr0h;
	resultaat = (resultaat << 2) | (adc_resr0l >> 6);
	syscon0 = tmpStack[0];
	adc_page = tmpStack[1];
	return resultaat;
}

//*************************************************************************************************
//
//! \brief Meet de spanning van de potmeter
//!
//! \return:
//! 	10 bit meting in een uint16_t
//
//*************************************************************************************************

uint16_t adcpotmeter(void){
	return getadc(4);
}

//*************************************************************************************************
//
//! \brief Meet de spanning van de lm335 temperatuursensor
//!
//! \return:
//! 	10 bit meting in een uint16_t

//
//*************************************************************************************************

uint16_t adclm335(void){
	return getadc(5);
}

//! @}
//! \addtogroup XC888_PWM XC888 PWM
//! @{
//! \brief Alles wat met pwm te maken heeft

//*************************************************************************************************
//
//! \brief Zal t12,p3.5,p3.3 en p3.1 juist zetten voor 8 bit pwm  
//! 
//! \todo Documenteren welke frequentie de pwm heeft en hoe men deze kan aanpassen.
//
//*************************************************************************************************

void initpwm (void){
	uint8_t tmpStack[3];

	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;
	tmpStack[2] = ccu6_page;
	
	//De Poort juist zetten.
	syscon0 &= 0b11111110;		//clear bit rmap
	port_page = 0x00;			//basis regs kiezen
	p3_dir |= 0b00101010;		//alle pinnen als output
	port_page = 0x02;			//altsel regs beschikbaar maken
	p3_altsel0 = 0b00101010;	//p3.5, p3.3 en p3.1 als outputs voor pwm gebruiken
	p3_altsel1 = 0b00000000;	

	//Timer 12 klaarzetten
	ccu6_page = 0x02;			//ccu6_12tmsel h en l bereikbaar maken
	ccu6_t12mselh = 0b00000010;	//compare output op cout62
	ccu6_t12msell = 0b00100010;	//idem cout61 en cout60
	ccu6_modctrl = 0b00101010;	//modulatie uitgangssignaal toelaten
	ccu6_page = 0x01;
	ccu6_t12prl = 0xff;			//timer in 8 bit mode gebruiken
	ccu6_t12prh = 0x00;
	ccu6_tctr0l = 0b00000000;	//prescalers instellen, kan enkel als timer stil staat
	//LET OP, in dit register de run bit zetten of shadow transfer enable heeft geen zin 
	//(read only bits.) Moet gebeuren via ccu6_tctr4l
	ccu6_page = 0;
	ccu6_tctr4l |= 0b01000010;	//shadow transfer nog uitvoeren, en timer starten

	syscon0 = tmpStack[0];
	port_page = tmpStack[1];
	ccu6_page = tmpStack[2];
}

//*************************************************************************************************
//
//! \brief Zal de pwm waarde updaten  
//! 
//! \param channel Hiermee kan men het kanaal selecteren 
//! - 0 voor p3.1 
//! - 1 voor p3.3 
//! - 2 voor p3.5
//!
//! \param setValue Hiermee kan je het kantelpunt van de PWM instellen.
//! deze moet liggen tussen 0 en 255. 
//
//*************************************************************************************************

void pwmset (uint8_t channel, uint8_t setValue){
	uint8_t tmpStack[2];

	tmpStack[0] = syscon0;
	tmpStack[1] = ccu6_page;

	syscon0 &= 0b11111110;		//clear bit rmap
	ccu6_page = 0;
	if(2 == channel){
		ccu6_cc62srh = 0;
		ccu6_cc62srl = setValue;
	}
	if(1 == channel){
		ccu6_cc61srh = 0;
		ccu6_cc61srl = setValue;
	}
	if(0 == channel){
		ccu6_cc60srh = 0;
		ccu6_cc60srl = setValue;
	
	}
	ccu6_tctr4l |= 0b01000000;	//shadow transfer nog uitvoeren
	syscon0 = tmpStack[0];
	ccu6_page = tmpStack[1];
}

//! @}
//! \addtogroup XC888_SPI XC888 SPI
//! @{
//! \brief Alles wat met spi te maken heeft

//*************************************************************************************************
//
//! \brief Zal de spi initializeren als fullduplex master 
//! 
//! P1.2 is SCLK, P1.3 is MOSI, P1.4 is MISO, P1.5 is CS 
//!	\param mode Bepaald de mode waarin de SPI werkt. 
//! deze parameter moet 1 van volgende defines zijn:
//!	\b SPI_MODE00, \b SPI_MODE01, \b SPI_MODE10, \b SPI_MODE11 <br>
//! De 00 01 10 en 11 is een tuple van CPOL en CPHA 
//! (zie defines SPI_MODExx voor meer info en/of 
//! https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus)
//! \param heading Deze bepaald welke bit eerst gezonden wordt. 
//! \b SPI_LSB_FIRST LSB eerst, \b SPI_MSB_FIRST MSB eerst.
//! \param speed Bepaald de snelheid van de SPI. 
//! Voor het gemak zijn er de volgende defines om te gebruikten:
//! - \b SPI_12M_BAUD
//! - \b SPI_6M_BAUD
//! - \b SPI_1M3_BAUD
//! - \b SPI_1M_BAUD
//! - \b SPI_750K_BAUD
//! - \b SPI_666K7_BAUD
//! - \b SPI_600K_BAUD
//! - \b SPI_500K_BAUD
//! - \b SPI_266K7_BAUD
//! - \b SPI_200K_BAUD
//! - \b SPI_133K3_BAUD
//! - \b SPI_100K_BAUD	
//
//*************************************************************************************************

void initspi (uint8_t mode, uint8_t heading, uint8_t speed){
	uint8_t tmpStack[2];

	tmpStack[0] = syscon0;
	tmpStack[1] = port_page;

	syscon0 = 0x04;
	port_page = 0x00;
	p1_dir = 0b00101100;		//juiste pinnen als output schakelen (1)
	port_page = 0x02;			//altsel registers beschikbaar maken
	p1_altsel0 = 0b00011100;	//alt functies SPI
	p1_altsel1 = 0b00000000;	//alt functies SPI
	ssc_brh = 0x00;				//baud rate waarde inladen
	ssc_brl = speed;				//idem
	ssc_conl = 0x07;			//8 bit data
	if(heading){
		ssc_conl |= 1 << 4;		//MSB first
	}
	if(mode & 0b10){
		ssc_conl |= 1 << 6;		//een klok in rust is hoog
	}
	if(!(mode & 0b1)){
		//Bij CPHA = 0 moet PH = 1 Latch receive data on leading clock edge, shift on trailing edge
		ssc_conl |= 1 << 5;		
	}
	//ssc_conh = 0b01000000;	//stop mode (uitgeschakelde SPI)
	ssc_conh = 0b11000000;		//active mode (inschakelen SPI) + mastermode 
	
	syscon0 = tmpStack[0];
	port_page = tmpStack[1];

}

//*************************************************************************************************
//
//! \brief Zal een byte via spi naar buiten sturen (en terwijl ook een byte lezen.
//! 
//! Aangezien SPI een fullduplex bus is zal er tijdens het zenden ook een byte worden ingelezen
//! \param dataByte De te verzenden byte
//! \return De ontvangen byte. (Mag genegeerd worden als je hem niet nodig hebt).
//
//*************************************************************************************************

uint8_t spioutbyte(uint8_t dataByte){
	ssc_tbl = dataByte;			//data verzenden
	while(ssc_conh & (1 << 4));	//wachten tot het weg is
	return ssc_rbl;
}

//! @}
//! \addtogroup XC888_INT XC888 Interrupts
//! @{
//! \brief Alles wat met interrupts te maken heeft
//!
//! \warning Opgepast met interrupts! Het opent de deur voor een paar intresante bugs
//! Hieronder staat wat de manual van sdcc er van zegt:
//! 
//! 3.8.1.1    Common interrupt pitfall: variable not declared volatile
//! If an interrupt service routine changes variables which are accessed by other functions these variables have to be
//! declared volatile. See http://en.wikipedia.org/wiki/Volatile_variable .
//! 
//! 3.8.1.2    Common interrupt pitfall: non-atomic access
//! If the access to these variables is not atomic (i.e.  the processor needs more than one instruction for the access
//! and could be interrupted while accessing the variable) the interrupt must be disabled during the access to avoid
//! inconsistent data. Access  to  16  or  32  bit  variables  is  obviously  not  atomic  on  8  bit  CPUs  
//! and  should  be  protected  by  disabling interrupts.  
//! You're not automatically on the safe side if you use 8 bit variables though.  We need an example here:
//! f.e.  on the 8051 the harmless looking "flags |= 0x80;"  is not atomic if flags resides in xdata.  
//! Setting "flags |= 0x40;" from within an interrupt routine might get lost if the interrupt occurs at the wrong time.
//! "counter += 8;" is not atomic on the 8051 even if counter is located in data memory. 
//! Bugs like these are hard to reproduce and can cause a lot of trouble.
//! 
//! 3.8.1.3    Common interrupt pitfall:
//! stack overflow
//! The return address and the registers used in the interrupt service routine are saved on the stack so there must be
//! sufficient stack space. If there isn't variables or registers (or even the return address itself) will be corrupted. 
//! This stack overflow is most likely to happen if the interrupt occurs during the "deepest" subroutine when the stack is
//! already in use for f.e. many return addresses.
//! 
//! 3.8.1.4    Common interrupt pitfall:
//! use of non-reentrant functions A special note here, int (16 bit) and long (32 bit) integer division, 
//! multiplication & modulus and floating-point operations are implemented using external support routines. 
//! If an interrupt service routine needs to do any of these operations then the support routines 
//! (as mentioned in a following section) will have to be recompiled using the --stack-auto option 
//! and the source file will need to be compiled using the --int-long-reent compiler option.
//! Note, the type promotion required by ANSI C can cause 16 bit routines to be used without the programmer being 
//! aware of it. See f.e. the cast
//! (unsigned char)(tail-1) within the if clause in section 3.11.2.
//! 
//! Calling other functions from an interrupt service routine is not recommended, avoid it if possible.  Note that
//! when some function is called from an interrupt service routine it should be preceded by a # pragma nooverlay if it is
//! not reentrant.  Furthermore nonreentrant functions should not be called from the main program while the interrupt
//! service routine might be active.  They also must not be called from low priority interrupt service routines while a
//! high priority interrupt service routine might be active.  You could use semaphores or make the function critical
//! if all parameters are passed in registers. Also see section 3.7 about Overlaying and section 3.10
//! about Functions using private register banks.
//! 

//! @}
//! @}